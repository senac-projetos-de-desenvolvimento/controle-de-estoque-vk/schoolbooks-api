import express = require('express');
import { SchemaBuilder } from 'knex';
const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
const schema : SchemaBuilder = buildSchema(`
  type Query {
    hello: String,
    time: Int,
    isHappy: Boolean!
  }
`);

// The root provides a resolver function for each API endpoint
const root  = {
  hello: () => {
    return 'É os guri programando em Graphql bem quentaque';
  },
};

const app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');