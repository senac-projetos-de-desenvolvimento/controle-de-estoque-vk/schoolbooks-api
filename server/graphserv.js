"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var graphqlHTTP = require('express-graphql').graphqlHTTP;
var buildSchema = require('graphql').buildSchema;
// Construct a schema, using GraphQL schema language
var schema = buildSchema("\n  type Query {\n    hello: String,\n    time: Int,\n    isHappy: Boolean!\n  }\n");
// The root provides a resolver function for each API endpoint
var root = {
    hello: function () {
        return 'É os guri programando em Graphql bem quentaque';
    },
};
var app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');
